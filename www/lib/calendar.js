function NepaliCalendar() {
    this.bs = [
        [2000, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2001, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2002, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2003, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2004, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2005, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2006, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2007, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2008, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
        [2009, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2010, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2011, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2012, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
        [2013, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2014, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2015, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2016, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
        [2017, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2018, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2019, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2020, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
        [2021, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2022, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
        [2023, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2024, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
        [2025, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2026, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2027, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2028, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2029, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30],
        [2030, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2031, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2032, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2033, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2034, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2035, 30, 32, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
        [2036, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2037, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2038, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2039, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
        [2040, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2041, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2042, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2043, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
        [2044, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2045, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2046, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2047, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
        [2048, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2049, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
        [2050, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2051, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
        [2052, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2053, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
        [2054, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2055, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2056, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30],
        [2057, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2058, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2059, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2060, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2061, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2062, 30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31],
        [2063, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2064, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2065, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2066, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
        [2067, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2068, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2069, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2070, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
        [2071, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2072, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
        [2073, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
        [2074, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
        [2075, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2076, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
        [2077, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
        [2078, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
        [2079, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
        [2080, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
        [2081, 31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30],
        [2082, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
        [2083, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30],
        [2084, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30],
        [2085, 31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30],
        [2086, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
        [2087, 31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30],
        [2088, 30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30],
        [2089, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
        [2090, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30]
    ];
    this.nep_date = {
        "year": "",
        "month": "",
        "date": "",
        "day": "",
        "nmonth": "",
        "num_day": ""
    };
    this.eng_date = {
        "year": "",
        "month": "",
        "date": "",
        "day": "",
        "nmonth": "",
        "num_day": ""
    };
    this.debug_info = '';
    this.debug = true;
}

NepaliCalendar.prototype.is_leap_year = function(year) {
    if (year % 100 === 0) {

        return year % 400 === 0

    } else {

        return year % 4 === 0
    }
};

NepaliCalendar.prototype.get_nepali_month = function(m) {
    var months = [
        "",
        "Baishak",
        "Jestha",
        "Ashad",
        "Shrawn",
        "Bhadra",
        "Ashwin",
        "kartik",
        "Mangshir",
        "Poush",
        "Magh",
        "Falgun",
        "Chaitra"
    ];
    if (m < 1 || m > 12) return false;
    return months[m];

};

NepaliCalendar.prototype.get_english_month = function(m) {
    var months = ["",
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"

    ];
    if (m < 1 || m > 12) return false;
    return months[m];

};

NepaliCalendar.prototype.get_day_of_week = function(d) {
    var week_days = ["",
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ];
    if (d < 1 || d > 7) return false;
    return week_days[d];

};

NepaliCalendar.prototype.is_range_eng = function(yy, mm, dd) {
    if (yy < 1944 || yy > 2033) {
        this.debug_info = "Supported only between 1944-2022";
        return false;
    }

    if (mm < 1 || mm > 12) {
        this.debug_info = "Error! value 1-12 only";
        return false;
    }

    if (dd < 1 || dd > 31) {
        this.debug_info = "Error! value 1-31 only";
        return false;
    }

    return true;
};

NepaliCalendar.prototype.is_range_nep = function(yy, mm, dd) {
    if (yy < 2000 || yy > 2089) {
        this.debug_info = "Supported only between 2000-2089";
        return false;
    }

    if (mm < 1 || mm > 12) {
        this.debug_info = "Error! value 1-12 only";
        return false;
    }

    if (dd < 1 || dd > 32) {
        this.debug_info = "Error! value 1-32 only";
        return false;
    }

    return true;
};
/**
 * currently can only calculate the date between AD 1944-2033...
 *
 * @param unknown_type $yy
 * @param unknown_type $mm
 * @param unknown_type $dd
 * @return unknown
 */

NepaliCalendar.prototype.eng_to_nep = function($yy, $mm, $dd) {
    this.debug && console.time('eng_to_nep');
    var $this = this;

    if ($this.is_range_eng($yy, $mm, $dd) == false) {
        return false;
    } else {

        // english month data.
        var $month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
            $lmonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

            $def_eyy = 1944, //spear head english date...
            $def_nyy = 2000,
            $def_nmm = 9,
            $def_ndd = 17 - 1, //spear head nepali date...
            $total_eDays = 0,
            $total_nDays = 0,
            $a = 0,
            $day = 7 - 1, //all the initializations...
            $m = 0,
            $y = 0,
            $i = 0,
            $j = 0,
            $numDay = 0;

        // count total no. of days in-terms of year
        for ($i = 0; $i < ($yy - $def_eyy); $i++) { //total days for month calculation...(english)
            if ($this.is_leap_year($def_eyy + $i) == 1)
                for ($j = 0; $j < 12; $j++)
                    $total_eDays += $lmonth[$j];
            else
                for ($j = 0; $j < 12; $j++)
                    $total_eDays += $month[$j];
        }

        // count total no. of days in-terms of month
        for ($i = 0; $i < ($mm - 1); $i++) {
            if ($this.is_leap_year($yy) == 1)
                $total_eDays += $lmonth[$i];
            else
                $total_eDays += $month[$i];
        }

        // count total no. of days in-terms of date
        $total_eDays += $dd;


        $i = 0;
        $j = $def_nmm;
        $total_nDays = $def_ndd;
        $m = $def_nmm;
        $y = $def_nyy;

        // count nepali date from array
        while ($total_eDays != 0) {
            $a = $this.bs[$i][$j];
            $total_nDays++; //count the days
            $day++; //count the days interms of 7 days
            if ($total_nDays > $a) {
                $m++;
                $total_nDays = 1;
                $j++;
            }
            if ($day > 7)
                $day = 1;
            if ($m > 12) {
                $y++;
                $m = 1;
            }
            if ($j > 12) {
                $j = 1;
                $i++;
            }
            $total_eDays--;
        }

        $numDay = $day;

        $this.nep_date["year"] = $y;
        $this.nep_date["month"] = $m;
        $this.nep_date["date"] = $total_nDays;
        $this.nep_date["day"] = $this.get_day_of_week($day);
        $this.nep_date["nmonth"] = $this.get_nepali_month($m);
        $this.nep_date["num_day"] = $numDay;
        this.debug && console.timeEnd('eng_to_nep');
        return $this.nep_date;
    }
}


/**
 * currently can only calculate the date between BS 2000-2089
 *
 * @param unknown_type $yy
 * @param unknown_type $mm
 * @param unknown_type $dd
 * @return unknown
 */
NepaliCalendar.prototype.nep_to_eng = function($yy, $mm, $dd) {
    this.debug && console.time('nep_to_eng');

    var
        $this = this,
        $def_eyy = 1943,
        $def_emm = 4,
        $def_edd = 14 - 1,
    // init english date.
        $def_nyy = 2000,
        $def_nmm = 1,
        $def_ndd = 1,
    // equivalent nepali date.
        $total_eDays = 0,
        $total_nDays = 0,
        $a = 0,
        $day = 4 - 1,
    // initializations...
        $m = 0,
        $y = 0,
        $i = 0,
        $j = 0,
        $k = 0,
        $numDay = 0,

        $month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        $lmonth = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if ($this.is_range_nep($yy, $mm, $dd) === false) {
        return false;

    } else {

        // count total days in-terms of year
        for ($i = 0; $i < ($yy - $def_nyy); $i++) {
            for ($j = 1; $j <= 12; $j++) {
                $total_nDays += $this.bs[$k][$j];
            }
            $k++;
        }

        // count total days in-terms of month
        for ($j = 1; $j < $mm; $j++) {
            $total_nDays += $this.bs[$k][$j];
        }

        // count total days in-terms of dat
        $total_nDays += $dd;

        //calculation of equivalent english date...
        $total_eDays = $def_edd;
        $m = $def_emm;
        $y = $def_eyy;
        while ($total_nDays != 0) {
            if ($this.is_leap_year($y)) {
                $a = $lmonth[$m];
            } else {
                $a = $month[$m];
            }
            $total_eDays++;
            $day++;
            if ($total_eDays > $a) {
                $m++;
                $total_eDays = 1;
                if ($m > 12) {
                    $y++;
                    $m = 1;
                }
            }
            if ($day > 7)
                $day = 1;
            $total_nDays--;
        }
        $numDay = $day;

        $this.eng_date["year"] = $y;
        $this.eng_date["month"] = $m;
        $this.eng_date["date"] = $total_eDays;
        $this.eng_date["day"] = $this.get_day_of_week($day);
        $this.eng_date["emonth"] = $this.get_english_month($m);
        $this.eng_date["num_day"] = $numDay;
        this.debug && console.timeEnd('nep_to_eng');
        return $this.eng_date;
    }
}