angular.module('starter')
    .factory("RequestGatePass", function($q, $http, localStorageService) {

        function get_request_form(credentails) {

            var deferred = $q.defer();
            var credentails = localStorageService.get('LoginInfo');
            $http({
                method: "GET",
                url: getBaseUrl() + 'api/GatePass',
                headers: { Authorization: 'Bearer ' + credentails.access_token }
            }).success(function mySucces(data) {
                deferred.resolve(data);
            }).error(function(err) {
                console.log(err);
            });
            return deferred.promise;
        }

        function postgatePassData(datas) {
            

            var deferred = $q.defer();
            var info = localStorageService.get('LoginInfo');
            $http({
                method: 'POST',
                url: getBaseUrl() + 'api/GatePass/create',
                headers: { Authorization: 'Bearer ' + info.access_token },
                data: datas
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(err) {
                deferred.resolve(err);
            })
            return deferred.promise;

        }

        function update_employee_for_gate_pass(entry_nos, employee_code, gate_pass_reason) {
            var deferred = $q.defer();
            var info = localStorageService.get('LoginInfo');
            $http({
                method: 'POST',
                url: getBaseUrl() + 'api/GatePass/UpdateEmployeeForGatePass?entryCode=' + entry_nos + '&employeeNumber=' + employee_code + '&gatePassReason=' + gate_pass_reason,
                headers: { Authorization: 'Bearer ' + info.access_token }
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(err) {
                deferred.resolve(err);
            })
            return deferred.promise;
        }

        function gate_pass_date_update(entry_nos, option, date_value) {
            var deferred = $q.defer();
            var info = localStorageService.get('LoginInfo');
            $http({
                method: 'POST',
                url: getBaseUrl() + 'api/GatePassDateUpdate?entryCode=' + entry_nos + '&option=' + option + '&value=' + date_value,
                headers: { Authorization: 'Bearer ' + info.access_token }
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(err) {
                deferred.resolve(err);
            })
            return deferred.promise;
        }

        function update_gate_pass(entry_nos, update_param, update_value) {
            var deferred = $q.defer();
            var info = localStorageService.get('LoginInfo');
            if (update_param == 'StartDate') {
                $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/GatePassRequestUpdate/UpdateDate',
                    headers: { Authorization: 'Bearer ' + info.access_token },
                    data: {
                        Entry_No: entry_nos,
                        UpdateParameter: update_param,
                        StartDate: update_value

                    }
                }).success(function(response) {
                    deferred.resolve(response);
                }).error(function(err) {
                    deferred.resolve(err);
                })
            } else if (update_param == 'EndDate') {
                $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/GatePassRequestUpdate/UpdateDate',
                    headers: { Authorization: 'Bearer ' + info.access_token },
                    data: {
                        Entry_No: entry_nos,
                        UpdateParameter: update_param,
                        EndDate: update_value

                    }
                }).success(function(response) {
                    deferred.resolve(response);
                }).error(function(err) {
                    deferred.resolve(err);
                })
            } else if (update_param == 'StartTime') {
                $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/GatePassRequestUpdate/UpdateDate',
                    headers: { Authorization: 'Bearer ' + info.access_token },
                    data: {
                        Entry_No: entry_nos,
                        UpdateParameter: update_param,
                        StartTime: update_value

                    }
                }).success(function(response) {
                    deferred.resolve(response);
                }).error(function(err) {
                    deferred.resolve(err);
                })
            } else if (update_param == 'EndTime') {
                $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/GatePassRequestUpdate/UpdateDate',
                    headers: { Authorization: 'Bearer ' + info.access_token },
                    data: {
                        Entry_No: entry_nos,
                        UpdateParameter: update_param,
                        EndTime: update_value

                    }
                }).success(function(response) {
                    deferred.resolve(response);
                }).error(function(err) {
                    deferred.resolve(err);
                })
            }
            return deferred.promise;
        }

        return {
            get_request_form: get_request_form,
            postgatePassData: postgatePassData,
            update_employee_for_gate_pass: update_employee_for_gate_pass,
            gate_pass_date_update: gate_pass_date_update,
            update_gate_pass: update_gate_pass
        }





    });

angular.module('starter')
    .factory("PendingGatePassRequestList", function($q, $http, localStorageService) {
        var credentails = localStorageService.get('LoginInfo');
        function get_assistant_employee_gatePassList() {
         

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: getBaseUrl() + 'api/PendingGatePassRequestofEmployee',
                headers: { Authorization: 'Bearer ' + credentails.access_token }
            }).success(function(data) {

                deferred.resolve(data);
            }).error(function(err) {
                
                deferred.resolve(err);
            })
            return deferred.promise;
        }



        function get_pending_gatePass_byId(id) {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                //url: getBaseUrl()+'api/gatepass/GatePassRequestByEntryNumber?EntryNumber='+id,
                url: getBaseUrl() + 'api/PendingGatePassRequestOfEmployee/GetGatePassRequestHistory?entryNumber=' + id,
                headers: { Authorization: 'Bearer ' + credentails.access_token },
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(err) {
                deferred.resolve(err);
            })
            return deferred.promise;

        }

        function gatePassApproveDisapprove(Entry_No, status, remarks) {
            
            if (status == 'Approve') {
                var url = getBaseUrl() + 'api/' + status + 'GatePassRequest/' + status + '?entryNumber=' + Entry_No + '&remarks=' + remarks;
            } else {
                var url = getBaseUrl() + 'api/' + status + 'GatePassRequest/' + status + '?entryNumber=' + Entry_No;
            }
            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    url: url,
                    headers: { Authorization: 'Bearer ' + credentails.access_token },
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })

            return deferred.promise;


        }





        return {
            get_assistant_employee_gatePassList: get_assistant_employee_gatePassList,
            get_pending_gatePass_byId: get_pending_gatePass_byId,
            gatePassApprroveDisapprove: gatePassApproveDisapprove
        }
    });


angular.module('starter')
    .factory("gatePassHistory", function($q, $http, localStorageService) {

        function get_gatePasshistory(credentails) {


            var deferred = $q.defer();
            var gatePassHistory = localStorageService.get('gatePassHistoryRecords');
    
            if (gatePassHistory == null || gatePassHistory.length==0) {

                $http({
                        method: 'GET',
                        url: getBaseUrl() + 'api/GatePassHistory',
                        headers: { Authorization: 'Bearer ' + credentails.access_token },
                    })
                    .success(function(data) {
                        deferred.resolve(data);
                    })
                    .error(function(err) {
                        deferred.resolve(err);
                    })

            } else {

                deferred.resolve(gatePassHistory);

            }


            return deferred.promise;
        }
        return {
            get_gatePasshistory: get_gatePasshistory
        }
    });
