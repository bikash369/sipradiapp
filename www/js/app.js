// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'LocalStorageModule', 'ngRoute', 'ionic-datepicker', 'ionic-timepicker'])

.run(function($ionicPlatform, $cordovaStatusbar, LoginService, $rootScope, $state) {

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromparams) {

        //Always prompt login if not logged in


        if (toState.authRequired && LoginService.isAuthenticated() == null) {
            $state.go('login');
            event.preventDefault();


        }

    });


    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.cordova) {
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            $cordovaStatusbar.overlaysWebView(true);
            $cordovaStatusbar.styleHex('#000000');
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'MenuCtrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: "templates/login.html",
            controller: 'LoginCtrl'



        })
        .state('app.dash', {
            url: '/dashboard',
            authRequired: true,
            views: {
                'menuContent': {
                    templateUrl: 'templates/home.html',
                    controller: 'HomeCtrl'

                }
            }
        })



    .state('app.request-leave', {
        url: '/request-leave',
        authRequired: true,
        cache: false,

        views: {
            'menuContent': {
                templateUrl: 'templates/request-leave.html',
                controller: 'RequestLeaveCtrl'
            }
        }
    })

    .state('app.request-gate-pass', {
        url: '/request-gate-pass',
        authRequired: true,
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/request-gate-pass.html',
                controller: 'GatePassCtrl'
            }
        }
    })

    .state('app.request-odd-trainning', {
        url: '/request-odd-trainning',
        authRequired: true,
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/request-odd-trainning.html',
                controller: 'RequestOddTrainingCtrl'
            }
        }
    })

    .state('app.pending-leave-request', {
        url: '/pending-leave-request',
        authRequired: true,
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/pending-leave-request.html',
                controller: "PendingLeaveRequestCtrl"
            }
        }
    })

    .state('app.pending-gate-pass', {
        url: '/pending-gate-pass',
        authRequired: true,
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/pending-gate-pass.html',
                controller: 'PendingGatePassCtrl'
            }
        }
    })

    .state('app.pending-odd-trainning', {
        url: '/pending-odd-trainning',
        authRequired: true,
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/pending-odd-trainning.html',
                controller: 'PendingOddTrainningCtrl'
            }
        }
    })



    .state('app.employee-detail', {
        url: '/employee-detail',
        authRequired: true,
        views: {
            'menuContent': {
                templateUrl: 'templates/employee-detail.html',
                controller: 'HomeCtrl'
            }
        }
    })


    .state('app.pending-leave-request-approval/:emp_id', {
            url: '/pending-leave-request-approval/:emp_id',
            authRequired: true,
            views: {
                'menuContent': {
                    templateUrl: 'templates/pending-leave-request-approval.html',
                    controller: 'PendingLeaveRequestCtrl'
                }
            }
        })
        .state('app.pending-gate-pass-approval/:emp_id', {
            url: '/pending-gate-pass-approval/:emp_id',
            authRequired: true,
            views: {
                'menuContent': {
                    templateUrl: 'templates/pending-gate-pass-approval.html',
                    controller: 'PendingGatePassCtrl'
                }
            }
        })

    .state('app.pending-odd-trainning-approval/:emp_id', {
            url: '/pending-odd-trainning-approval/:emp_id',
            authRequired: true,
            views: {
                'menuContent': {
                    templateUrl: 'templates/pending-odd-trainning-approval.html',
                    controller: 'PendingOddTrainningCtrl'
                }
            }
        })
        .state('app.leave-history', {
            url: '/leave-history',
            authRequired: true,
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'templates/leave-history.html',
                    controller: 'leaveHistoryCtrl'
                }
            }
        })
        .state('app.gate-pass-history', {
            url: '/gate-pass-history',
            authRequired: true,
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'templates/gate-pass-history.html',
                    controller: 'gatePassHistoryCtrl'
                }
            }
        })

    .state('app.odd-trainning-history', {
        url: '/odd-trainning-history',
        authRequired: true,
        cache: false,

        views: {
            'menuContent': {
                templateUrl: 'templates/odd-trainning-history.html',
                controller: 'oddTrainningHistoryCtrl'
            }
        }
    })


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
})


.config(function(ionicTimePickerProvider) {
    var timePickerObj = {
        inputEpochTime: ((new Date()).getHours() * 60 * 60),
        format: 12,
        step: 1,
        setLabel: 'Set',
        closeLabel: 'Close',
        setButtonType: 'button-positive', //Optional
        closeButtonType: 'button-stable' //Optional

    }
    ionicTimePickerProvider.configTimePicker(timePickerObj);
});
