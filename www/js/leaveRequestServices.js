angular.module('starter')
    .factory("LoginService", function($q, $http, localStorageService, $location) {

        function get_company() {
            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: getBaseUrl() + 'api/Company',
                })
                .success(function(data) {

                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function login(user) {
            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    url: getBaseUrl() + 'Token',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function(obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: {
                        grant_type: 'password',
                        username: user.username,
                        password: user.password,
                        scope: user.company
                    }
                })
                .success(function(data) {

                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function getDetails(credentails) {

            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: getBaseUrl() + 'api/employee/GetLoggedInEmployeeDetail',
                    headers: { Authorization: 'Bearer ' + credentails.access_token }
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    console.log(err);
                })
            return deferred.promise;
        }

        function check_login() {
            var deferred = $q.defer();
            var u_id = localStorageService.get('LoginInfo');
            if (u_id == null) {
                $location.path('/login');
            }
            return deferred.promise;

        }

        function isAuthenticated() {
            var deferred = $q.defer();
            var u_id = localStorageService.get('LoginInfo');
            return u_id;
            return deferred.promise;

        }

        function check_loggedIn() {
            var deferred = $q.defer();
            var u_id = localStorageService.get('LoginInfo');
            if (u_id == null) {
                $location.path('/login');
            } else {
                $location.path('/dashboard')
            }
            return deferred.promise;

        }
        return {
            login: login,
            getDetails: getDetails,
            check_login: check_login,
            check_loggedIn: check_loggedIn,
            get_company: get_company,
            isAuthenticated: isAuthenticated
        }
    });


angular.module('starter')
    .factory("UserDetails", function($q, $http, localStorageService) {

        var credentails = localStorageService.get('LoginInfo');

        function get_details(credentails) {

            var deferred = $q.defer();
            var udetails = localStorageService.get('Userdetails');
            if (udetails == null) {

                $http({
                        method: 'GET',
                        url: getBaseUrl() + 'api/employee/GetLoggedInEmployeeDetail',
                        headers: { Authorization: 'Bearer ' + credentails.access_token }
                    })
                    .success(function(data) {

                        deferred.resolve(data);
                    })
                    .error(function(err) {
                        console.log(err);
                    })
            } else {
                deferred.resolve(udetails);
            }
            return deferred.promise;
        }

        function get_assistant_employees() {

            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: getBaseUrl() + 'api/AssistantEmployee',
                    headers: { Authorization: 'Bearer ' + credentails.access_token }
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function get_assistant_employee_leaveList() {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: getBaseUrl() + 'api/PendingleaveRequestofEmployee',
                headers: { Authorization: 'Bearer ' + credentails.access_token }
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(err) {
                deferred.resolve(err);
            })
            return deferred.promise;
        }

        function get_pending_leaveRequest_byId(id) {

            var deferred = $q.defer();
            $http({
                method: 'GET',
                //url: getBaseUrl()+'api/LeaveRequestNumber/LeaveRequestByLeaveRequestNumber?leaveRequestNumber='+id,
                url: getBaseUrl() + 'api/PendingLeaveRequestOfEmployee/GetPendingLeaveRequest?leaveRequestNumber=' + id,
                headers: { Authorization: 'Bearer ' + credentails.access_token },
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(err) {
                deferred.resolve(err);
            })
            return deferred.promise;

        }







        function leaveRequestApprroveDisapprove(Entry_No, status, remarks) {

            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    //url: getBaseUrl() + 'api/'+status+'GateLeaveRequest/'+status+'?leaveRequestNumber='+Entry_No,
                    url: getBaseUrl() + 'api/ApproveLeaveRequest/' + status + '?leaveRequestNumber=' + Entry_No + '&remarks=' + remarks,
                    headers: { Authorization: 'Bearer ' + credentails.access_token },
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })

            return deferred.promise;
        }



        function get_leave_balance() {
            console.log('api call');
            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: getBaseUrl() + 'api/LeaveBalance',
                    headers: { Authorization: 'Bearer ' + credentails.access_token },
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })

            return deferred.promise;
        }







        return {
            get_details: get_details,
            get_assistant_employees: get_assistant_employees,
            get_assistant_employee_leaveList: get_assistant_employee_leaveList,
            get_pending_leaveRequest_byId: get_pending_leaveRequest_byId,
            leaveRequestApprroveDisapprove: leaveRequestApprroveDisapprove,
            get_leave_balance: get_leave_balance,



        }
    });












angular.module('starter')
    .factory("RequestLeave", function($q, $http, localStorageService) {

        function get_request_form(loginInfo) {

            var deferred = $q.defer();
            var u_id = localStorageService.get('LoginInfo');
            var requestFormData = localStorageService.get('requestData');
            if (requestFormData == null || requestFormData.length==0) {
                $http({
                    method: 'GET',
                    url: getBaseUrl() + 'api/leavetype',
                    headers: { Authorization: 'Bearer ' + u_id.access_token }
                }).success(function(data) {


                    deferred.resolve(data);
                }).error(function() {
                    alert("error");
                });
            } else {
                deferred.resolve(requestFormData);
            }
            return deferred.promise;
        }

        function get_request_no(credentails) {


            var deferred = $q.defer();
            // var requestNo = localStorageService.get("RequestNo");
            // if (requestNo == null) {
            $http({
                method: 'GET',
                url: getBaseUrl() + "api/leaverequestnumber",
                headers: { Authorization: 'Bearer ' + credentails.access_token }
            }).success(function(data) {



                deferred.resolve(data);
            }).error(function() {
                alert("error");
            });
            // } else {
            //     deferred.resolve(requestNo);
            // }
            return deferred.promise;

        }

        function post_leave(datas) {

            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/leaverequestnumber/create',
                    headers: { Authorization: 'Bearer ' + datas.access_token },
                    data: {
                        Leave_Request_No: datas.Leave_Request_No,
                        Leave_Start_Date: datas.Start_Date,
                        Leave_End_Date: datas.End_Date,
                        Leave_Type_Code: datas.Leave_Type,
                        Sub_Leave_Type: datas.Sub_Leave_Type,
                        Leave_Description: datas.description,
                        Remarks: datas.remarks,
                        Leave_Start_Time: datas.Start_Time,
                        Leave_End_Time: datas.End_Time,
                        Employee_No: datas.EmployeeCode
                    }

                })
                .success(function(response) {

                    deferred.resolve(response);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function get_on_hand_leave(datas, request_nos, leave_code, emp_code) {

            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/LeaveRequestUpdate/UpdateEmployeeForLeaveRequest?leaveRequestNumber=' + request_nos + '&employeeCode=' + emp_code + '&leaveTypeCode=' + leave_code,
                    headers: { Authorization: 'Bearer ' + datas.access_token },
                })
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function get_leave_detail(datas, request_nos, sub_leave_code) {

            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    url: getBaseUrl() + 'api/LeaveRequestUpdate/UpdateLeaveSubTypeForLeaveRequest?leaveRequestNumber=' + request_nos + '&leaveSubType=' + sub_leave_code,
                    headers: { Authorization: 'Bearer ' + datas.access_token },
                })
                .success(function(response) {
                    deferred.resolve(response);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        return {
            get_request_form: get_request_form,
            get_request_no: get_request_no,
            post_leave: post_leave,
            get_on_hand_leave: get_on_hand_leave,
            get_leave_detail: get_leave_detail


        }

    });





// angular.module('starter')
//     .factory("RecentLeaveRecords", function($q, $http, localStorageService) {

//         function get_recent_records(credentails, date) {

//             var deferred = $q.defer();
//             var recentRecords = localStorageService.get('recentLeaveRecords');
//             if (recentRecords == null) {
//                 $http({
//                         method: 'POST',
//                         url: '',
//                         data: {
//                             username: credentails.login_id,
//                             token: credentails.token,
//                             current_date: date

//                         }
//                     })
//                     .success(function(data) {


//                         var data = [{ "date": "2016-10-20", "leave_type": "NL", "no_of_days": "5", "remarks": "test", "balance": "2" }, { "date": "2016-05-25", "leave_type": "AL", "no_of_days": "1", "remarks": "another test", "balance": "5" }]

//                         deferred.resolve(data);

//                     })
//                     .error(function(err) {
//                         console.log(err);
//                     })



//             } else {

//                 deferred.resolve(recentRecords);
//             }
//             return deferred.promise;
//         }
//         return {
//             get_recent_records: get_recent_records
//         }
//     });


angular.module('starter')
    .factory("LeaveHistory", function($q, $http, localStorageService) {

        function get_history(credentails) {
            var deferred = $q.defer();
            var leaveHisory = localStorageService.get('leaveHistoryRecords');
            // console.log(leaveHisory.length);
            if (leaveHisory == null || leaveHisory.length==0) {

                $http({
                        method: 'GET',
                        headers: { Authorization: 'Bearer ' + credentails.access_token },
                        url: getBaseUrl() + 'api/LeaveHistory',

                    })
                    .success(function(data) {
                        deferred.resolve(data);
                    })
                    .error(function(err) {
                        deferred.resolve(err);
                    })

            } else {
                deferred.resolve(leaveHisory);

            }

            return deferred.promise;
        }

        return {
            get_history: get_history
        }
    });


angular.module('starter')
    .value('NepaliCalendar', NepaliCalendar)
    .factory('NepaliEnglishDateConverter', function() {
        return NepaliCalendar;
    });
