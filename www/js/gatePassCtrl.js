'use strict';
var app = angular.module("starter");
app.controller('GatePassCtrl', ['$scope', '$state', 'RequestGatePass', 'localStorageService', '$cordovaDialogs', 'ionicDatePicker', 'ionicTimePicker', '$ionicPopup', function($scope, $state, RequestGatePass, localStorageService, $cordovaDialogs, ionicDatePicker, ionicTimePicker, $ionicPopup) {


    //Custom Select Tag
    var startDate = new Date();
    var current_date = startDate.getMonth() + 1 + "/" + startDate.getDate() + "/" + startDate.getFullYear();
    $scope.isAdmin = localStorageService.get("IsAdmin");
    if ($scope.isAdmin === true) {
        $("#date-shows1").css("display", "inline-block");
        $("#date-shows2").css("display", "inline-block");
    } else {
        $("#date-shows1").css("display", "none");
        $("#date-shows2").css("display", "none");

    }


    $scope.entrydata = {};
    var startDate = new Date();
    var loginInfo = localStorageService.get('LoginInfo');
    RequestGatePass.get_request_form(loginInfo).then(function(response) {
        localStorageService.set('EntryNos', response.entry_NoField);
        $scope.entrydata.Entry_No = response.entry_NoField;
        $scope.entrydata.gate_pass_reason = [{ code: '1', name: 'Lunch' }, { code: '2', name: 'Marketing' }, { code: '3', name: 'Official' }, { code: '4', name: 'Personal' }]

    });
    $scope.employeeList = localStorageService.get('EmployeeList');
    $scope.gatePassStartDate = {
        inputDate: new Date(),
        from: new Date(),
        callback: function(val) {
            var start_date = convert(new Date(val));
            angular.element(document.querySelector('#startDate')).val(start_date);
            $scope.GatePassEndDate.from = start_date; //Change the From of the Input Date
            startDate = new Date(start_date);
            var from_date = new Date($("#startDate").val());
            var to_date = new Date($("#endDate").val());
            if ($("#startTime").val() && $("#endTime").val() && $("#endDate").val()) {
                var diff = calculateDiffinTime12Hr($("#startDate").val(), $("#endDate").val(), $("#startTime").val(), $("#endTime").val());
                $('#min-time').val(diff + ' mins');
            }


            var entry_nos = localStorageService.get('EntryNos');
            RequestGatePass.update_gate_pass(entry_nos, 'StartDate', $('#startDate').val()).then(function(response) {

                if (response.Message) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: response.Message
                    });
                }
            })

            if (from_date > to_date) {
                $ionicPopup.alert({
                    title: 'Error!',
                    template: " Start Date can\'t be greater than end date"
                });


                return false;
            }







        }
    }
    $scope.openDatePicker3 = function() {

        ionicDatePicker.openDatePicker($scope.gatePassStartDate);

    }


    $scope.GatePassEndDate = {
        from: new Date(),
        callback: function(val) {
            var end_date = convert(new Date(val));
            angular.element(document.querySelector('#endDate')).val(end_date);
            
        if($("#startTime").val() && $("#endTime").val() && $("#startDate").val()) {
            var diff = calculateDiffinTime12Hr($("#startDate").val(), $("#endDate").val(), $("#startTime").val(), $("#endTime").val());
            $('#min-time').val(diff + ' mins');
        }
            var entry_nos = localStorageService.get('EntryNos');
            RequestGatePass.update_gate_pass(entry_nos, 'EndDate', $('#endDate').val()).then(function(response) {
                console.log(response);
                if (response.Message) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: response.Message
                    });

                }
            })

            // if (from_date > to_date) {
            //     $ionicPopup.alert({
            //         title: 'Error!',
            //         template: " Start Date can\'t be greater than end date"
            //     });


            //     return false;
            // }






        }
    }

    $scope.openDatePicker4 = function() {

        ionicDatePicker.openDatePicker($scope.GatePassEndDate);
    };


    var gatePassStartTime = {
        callback: function(val) { //Mandatory
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {
                var t = (new Date).clearTime()
                    .addSeconds(val)
                    .toString('H:mm');
                var start_time = timeTo12HrFormat(t);

                // var selectedTime = new Date(val * 1000);
                // var start_time = selectedTime.getUTCHours() + ':' + selectedTime.getUTCMinutes();
                angular.element(document.querySelector('#startTime')).val(start_time);
                var entry_nos = localStorageService.get('EntryNos');
                RequestGatePass.update_gate_pass(entry_nos, 'StartTime', $('#startTime').val()).then(function(response) {
                    if (response.Message) {
                        $ionicPopup.alert({
                            title: 'Error!',
                            template: response.Message
                        });

                    }
                })

                // if ($("#endTime").val()) {
                //     var start = angular.element(document.querySelector('#startTime')).val().split(':');
                //     var end = angular.element(document.querySelector('#endTime')).val().split(':');
                //     var diff = calculateDiffinTime(end, start);
                //     $('#min-time').val(diff + ' mins');


                // }
                if ($("#endTime").val() && $("#startDate").val() && $("#endDate").val()) {
                    var diff = calculateDiffinTime12Hr($("#startDate").val(), $("#endDate").val(), $("#startTime").val(), $("#endTime").val());
                    $('#min-time').val(diff + ' mins');
                }

            }
        }

    }
    $scope.openTimePicker3 = function() {
        ionicTimePicker.openTimePicker(gatePassStartTime);

    }

    var gatePassEndTime = {
        callback: function(val) { //Mandatory
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {
                var t = (new Date).clearTime()
                    .addSeconds(val)
                    .toString('H:mm');

                var end_time = timeTo12HrFormat(t);

                // var selectedTime = new Date(val * 1000);
                // var end_time = selectedTime.getUTCHours() + ':' + selectedTime.getUTCMinutes();
                angular.element(document.querySelector('#endTime')).val(end_time);
                var entry_nos = localStorageService.get('EntryNos');
                RequestGatePass.update_gate_pass(entry_nos, 'EndTime', $('#endTime').val()).then(function(response) {
                    if (response.Message) {
                        $ionicPopup.alert({
                            title: 'Error!',
                            template: response.Message
                        });

                    }
                })
                $scope.entrydata.EndTime = angular.element(document.querySelector('#endTime')).val();
                // if ($("#startTime").val()) {
                //     var start = angular.element(document.querySelector('#startTime')).val().split(':');
                //     var end = angular.element(document.querySelector('#endTime')).val().split(':');
                //     var diff = calculateDiffinTime(end, start);
                //     $('#min-time').val(diff + ' mins');
                // }
                if ($("#startTime").val() && $("#startDate").val() && $("#endDate").val()) {
                    var diff = calculateDiffinTime12Hr($("#startDate").val(), $("#endDate").val(), $("#startTime").val(), $("#endTime").val());
                    $('#min-time').val(diff + ' mins');
                }


            }
        }

    }
    $scope.openTimePicker4 = function() {
        ionicTimePicker.openTimePicker(gatePassEndTime);

    }


    //date converter function

    function convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }



    //getting gatepass and employee on change value and calling update api

    $scope.onChangePassReason = function() {
        $scope.entrydata.Gate_Pass_Reasons = angular.element(document.querySelector('#passReason')).val();
        var gate_pass_reason = angular.element(document.querySelector('#passReason')).val();
        var emp_code = angular.element(document.querySelector('#employee_code')).val();
        if (emp_code != '' && gate_pass_reason != '') {

            var entry_nos = localStorageService.get('EntryNos');

            RequestGatePass.update_employee_for_gate_pass(entry_nos, emp_code, gate_pass_reason).then(function(response) {


                if (response.Message) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: response.Message
                    })
                } else {
                    // var start_time_from_api = response.start_TimeField.split('T')[1].split(':');
                    // var end_time_from_api = response.end_TimeField.split('T')[1].split(':');
                    var startdate = response.StartDate.split('T')[0];
                    var enddate = response.EndDate.split('T')[0];
                    var starttime = response.StartTime;
                    var endtime = response.EndTime;

                    angular.element(document.querySelector('#startDate')).val(startdate);
                    angular.element(document.querySelector('#endDate')).val(enddate);
                    angular.element(document.querySelector('#startTime')).val(starttime);
                    angular.element(document.querySelector('#endTime')).val(endtime);
                    // $("#startDate").val(startdate);
                    // $("#endDate").val(enddate);
                    // $("#startTime").val(starttime);
                    // $("#endTime").val(endtime);

                    var diff = calculateDiffinTime12Hr(startdate, enddate, starttime, endtime);

                    $('#min-time').val(diff + ' mins');



                    // $("#startDate").val(response.start_DateField.split('T')[0]);
                    // $("#endDate").val(response.end_DateField.split('T')[0]);

                    // $("#startTime").val(start_time_from_api[0] + ":" + start_time_from_api[1]);
                    // $scope.entrydata.EndTime = end_time_from_api[0] + ":" + end_time_from_api[1];

                    /* calculate the time required from start_time and end_time */
                    // var diff = calculateDiffinTime(end_time_from_api, start_time_from_api);
                    // $('#min-time').val(diff + ' mins');
                    /* calculate the time required from start_time and end_time */
                }
            });
        } else {
            $ionicPopup.alert({
                title: 'Warning!',
                template: 'Employee must be selected!'
            });
            return false;
        }




    }


    $scope.onChangeEmployee = function() {

        var emp_code = angular.element(document.querySelector('#employee_code')).val();
        var gate_pass_reasons = $scope.entrydata.Gate_Pass_Reasons;

        if (emp_code != '' && (gate_pass_reasons !== undefined)) {

            var entry_nos = localStorageService.get('EntryNos');
            RequestGatePass.update_employee_for_gate_pass(entry_nos, emp_code, gate_pass_reasons).then(function(response) {
                if (response.Message) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: response.Message
                    });
                } else {
                    var startdate = response.StartDate.split('T')[0];
                    var enddate = response.EndDate.split('T')[0];
                    var starttime = response.StartTime;
                    var endtime = response.EndTime;
                    angular.element(document.querySelector('#startDate')).val(startdate);
                    angular.element(document.querySelector('#endDate')).val(enddate);
                    angular.element(document.querySelector('#startTime')).val(starttime);
                    angular.element(document.querySelector('#endTime')).val(endtime);
                    var diff = calculateDiffinTime12Hr(startdate, enddate, starttime, endtime);
                    $('#min-time').val(diff + ' mins');









                    // var start_time_from_api = response.start_TimeField.split('T')[1].split(':');
                    // var end_time_from_api = response.end_TimeField.split('T')[1].split(':');
                    // $("#startDate").val(response.start_DateField.split('T')[0]);
                    // $("#endDate").val(response.end_DateField.split('T')[0]);
                    // $("#startTime").val(start_time_from_api[0] + ":" + start_time_from_api[1]);
                    // $scope.entrydata.EndTime = end_time_from_api[0] + ":" + end_time_from_api[1];
                    // /* calculate the time required from start_time and end_time */
                    // var diff = calculateDiffinTime(end_time_from_api, start_time_from_api);
                    // $('#min-time').val(diff + ' mins');


                    /* calculate the time required from start_time and end_time */
                }
            });
            // } else {
            //     swal({
            //         title:'',
            //         text : "Gate Pass Reason is a must",
            //         type:'error'
            //     });
            //     return false;
        }

    }





    $scope.GatePassRequestSubmit = function(isValid) {
        if (isValid) {
            // delete $scope.entrydata['EmployeeCode'];
            // delete $scope.entrydata['Gate_Pass_Reasons'];
            var strt_dte = angular.element(document.querySelector('#startDate')).val();
            var end_dte = angular.element(document.querySelector('#endDate')).val();
            // console.log(strt_dte);
            // console.log(end_dte);

            if (strt_dte == '' || end_dte == '') {
                $ionicPopup.alert({
                    title: 'Error!',
                    template: "Start Date and/or End Date can\'t be empty"
                });

                return false;
            } else {
                var start = new Date(strt_dte);
                var end = new Date(end_dte);
                if (start > end) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: "Start Date can\'t be greater than end date"
                    });
                    return false;
                } else {

                    RequestGatePass.postgatePassData($scope.entrydata).then(function(response) {
                        if (response == 1) {
                            $ionicPopup.alert({
                                title: 'Sent!',
                                template: "Your request has been sent to the admin!"
                            }).then(function() {
                                $state.go('app.pending-gate-pass');

                            });
                        } else {
                            $ionicPopup.alert({
                                title: 'Not Sent!',
                                template: response.Message
                            })
                        }




                    });


                }
            }


        } else {
            $scope.submitted = true;
        }
    }

}]);



app.controller('PendingGatePassCtrl', ['$scope', '$rootScope', '$stateParams', 'localStorageService', 'PendingGatePassRequestList', '$ionicPopup', '$ionicLoading', '$state', function($scope, $rootScope, $stateParams, localStorageService, PendingGatePassRequestList, $ionicPopup, $ionicLoading, $state) {

    if (!$stateParams.emp_id) {
        //for ionic loader
        $scope.show = function() {
            $ionicLoading.show({
                template: '<p>Loading...</p><ion-spinner></ion-spinner>'
            });
        };

        $scope.hide = function() {
            $ionicLoading.hide();
        };

        $scope.numberOfItemsToDisplay = 10;
        $scope.pendingGatePasstlists = getData();

        function getData() {
            var a = [];
            $scope.show($ionicLoading);
            PendingGatePassRequestList.get_assistant_employee_gatePassList().then(function(response) {
                angular.forEach(response, function(item) {
                    a.push(item);

                });
            }).finally(function($ionicLoading) {

                $scope.hide($ionicLoading);
            });

            return a;
        }

        $scope.loadMore = function() {

            if ($scope.pendingGatePasstlists.length > $scope.numberOfItemsToDisplay)
                $scope.numberOfItemsToDisplay += 10; // load 20 more items
        }

    }


    /* detail of request-approval */
    if ($stateParams.emp_id != undefined) {
        PendingGatePassRequestList.get_pending_gatePass_byId($stateParams.emp_id).then(function(response) {
            $scope.gatePassLeaveDetails = response;

            $scope.MinTime = function(StartTime, End_time) {
                var start = StartTime.split(':');
                var end = End_time.split(':');
                start = parseInt(start[0] * 60) + parseInt(start[1]);
                end = parseInt(end[0] * 60) + parseInt(end[1]);
                return (end - start);
            }
        });

    }

    $scope.Approval = function(req_no, status, remarks) {

        if (remarks == undefined) {
            var remarks = "";
        }

        // console.log(req_no);
        // console.log(status);
        // console.log(remarks);


        PendingGatePassRequestList.gatePassApprroveDisapprove(req_no, status, remarks).then(function(response) {

            if (response == true) {
                $ionicPopup.alert({
                        title: 'Success!',
                        template: 'Successfully ' + status + ' application'
                    })
                    .then(function(res) {

                        $state.go('app.pending-gate-pass');


                    });
            } else {
                $ionicPopup.alert({
                    title: 'Error!',
                    template: response.Message
                });
            }
        });
    }







}]);

app.controller('gatePassHistoryCtrl', ['$scope', 'localStorageService', 'gatePassHistory', '$ionicLoading', '$http', function($scope, localStorageService, gatePassHistory, $ionicLoading, $http) {
    var employee_id = localStorageService.get('LoginInfo');
    $scope.gatePassHistory = [];
    $scope.show = function() {
        $ionicLoading.show({
            template: '<p>Loading...</p><ion-spinner></ion-spinner>'
        });
    };

    $scope.hide = function() {
        $ionicLoading.hide();
    };
    $scope.show($ionicLoading);

    gatePassHistory.get_gatePasshistory(employee_id).then(function(response) {


        angular.forEach(response, function(item) {

            $scope.gatePassHistory.push(item);
        });
        localStorageService.set('gatePassHistoryRecords', response);

    }).finally(function($ionicLoading) {
        $scope.hide($ionicLoading);

    });






    $scope.doRefresh = function() {
        localStorageService.remove('gatePassHistoryRecords');
        $scope.gatePassHistory = [];
        $scope.show($ionicLoading);
        $http({
                method: 'GET',
                url: getBaseUrl() + 'api/GatePassHistory',
                headers: { Authorization: 'Bearer ' + employee_id.access_token },
            })
            .success(function(data) {

                angular.forEach(data, function(item) {
                    $scope.gatePassHistory.push(item);
                });
                localStorageService.set('gatePassHistoryRecords', data);


            })
            .error(function(err) {
                alert(err)
            }).finally(function($ionicLoading) {
                $scope.hide($ionicLoading);

            });


        console.log($scope.gatePassHistory.length);


        $scope.$broadcast('scroll.refreshComplete');


    };










}]);
