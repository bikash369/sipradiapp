/*
app.run(function() {
    $(window).load(function(){
        $('.window-loader').hide();
    })
});
 */
function getBaseUrl() {
    //return 'http://192.168.88.166:5050/'; /* local */
    return 'http://10.97.7.254:2003/'; /* live */
}
/* calculate difference in nos of days */
function calculateNosofDays(to_date, from_date) {

   
    var timeDiff = Math.abs(to_date.getTime() - from_date.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;

    return diffDays;
}
/* calculate difference in nos of days */
/* calculate difference in total mins */
function calculateDiffinTime(end_time, start_time) {
    var start = parseInt(start_time[0] * 60) + parseInt(start_time[1]);
    var end = parseInt(end_time[0] * 60) + parseInt(end_time[1]);
    var diff = parseInt(end - start);

    return diff;
}
/* calculate difference in total mins */
/* date format */
function changeDateFormat(selected_date, date_format) {
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var date = "";
    var currentDateTime = new Date();

    if(selected_date == currentDateTime) {
        date = new Date();
    } else {
        date = new Date(selected_date);
    }
    if(date_format == 'monthName') {
        return monthNames[date.getMonth()] + " " + date.getDate() + "," + date.getFullYear();
    }
}
/* date format */

function calculateDiffinTime12Hr(startdate, enddate, starttime, endtime) {
    console.log(startdate);
    console.log(enddate);
    startdate = startdate.split('-');
    startdate = startdate[0] + '/' + startdate[1] + '/' + startdate[2];
    enddate = enddate.split('-');
    enddate = enddate[0] + '/' + enddate[1] + '/' + enddate[2];
    startdate = new Date(startdate + ' ' + starttime).getTime();
    enddate = new Date(enddate + ' ' + endtime).getTime();
    var diff = Math.abs(enddate - startdate);
    var date = new Date(diff).getTime();
    var mins = Math.round(diff / 60000);
    return mins;
   
}



//function to get AM/PM 12 hour format
function timeTo12HrFormat(time)
{   // Take a time in 24 hour format and format it in 12 hour format
    var time_part_array = time.split(":");
    var ampm = 'AM';

    if (time_part_array[0] >= 12) {
        ampm = 'PM';
    }

    if (time_part_array[0] > 12) {
        time_part_array[0] = time_part_array[0] - 12;
    }

    formatted_time = time_part_array[0] + ':' + time_part_array[1] + ' ' + ampm;

    return formatted_time;
}





