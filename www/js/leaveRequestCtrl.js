var app = angular.module('starter');

app.controller('MenuCtrl', ['$scope', '$state', 'localStorageService', function($scope, $state, localStorageService) {

    $scope.logout = function() {
        localStorageService.clearAll();
        $state.go('login');

    }


}]);

app.controller('LoginCtrl', ['$scope', '$cordovaStatusbar', '$state', 'LoginService', 'localStorageService', '$rootScope', '$ionicPopup', function($scope, $cordovaStatusbar, $state, LoginService, localStorageService, $rootScope, $ionicPopup) {
    $scope.loginData = {};
    document.addEventListener("deviceready", function() {
        if (window.cordova) {
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            $cordovaStatusbar.overlaysWebView(true);
            $cordovaStatusbar.styleHex('#2388B4');
        }
    }, false);


    LoginService.get_company().then(function(response) {
        console.log(response);
        $scope.companyList = response;
    });



    var loginInfo = localStorageService.get('LoginInfo');
    $scope.login = function(isValid) {
        if (isValid) {

            if (!$scope.loginData.company) {
                $ionicPopup.alert({
                    title: 'Required!',
                    template: 'Company is required'
                });

            } else {
                LoginService.login($scope.loginData).then(function(response) {



                    if (response.error) {

                        $ionicPopup.alert({
                            title: 'Invalid User!',
                            template: response.error_description
                        });
                    } else {
                        afterLoginSuccessfull(response);
                    }





                });

            }


        } else {
            $scope.submitted = true;
        }


    }

    function afterLoginSuccessfull(response) {

        if (response.employee_id) {
            var loginInfo = {
                access_token: response.access_token,
                token_type: response.token_type,
                login_id: response.employeeId,
                employee_id: response.employee_id
            };
            // console.log(loginInfo);
        } else {

            var loginInfo = {
                access_token: response.access_token,
                token_type: response.token_type,
                login_id: response.employeeId
            };



        }
        if (response) {
            localStorageService.set("LoginInfo", loginInfo);
            LoginService.getDetails(loginInfo).then(function(response) {
                localStorageService.set("Userdetails", response);
                localStorageService.set("IsAdmin", response.IsAdmin);

            });

            $state.go('app.dash')
        } else {
            $ionicPopup.alert({
                title: 'Invalid User!',
                template: response.error_description
            });

        }
    }
}]);


app.controller('HomeCtrl', ['$scope', 'UserDetails', 'localStorageService', function($scope, UserDetails, localStorageService) {
    var loginInfo = localStorageService.get('LoginInfo');
    $scope.details = localStorageService.get('Userdetails');
    $scope.leave_balances = localStorageService.get('leaveBalance');


    // $scope.logininfo = loginInfo;
    var current_date = new Date();
    // if ($scope.details == null) {
    UserDetails.get_details(loginInfo).then(function(response) {
        $scope.details = response;


        var blood_grp = response.blood_group.split("_");
        $scope.details.blood_group_name = blood_grp[0];

        if (blood_grp[1] != '') {
            $scope.details.blood_group_type = '+ve';
        } else {
            $scope.details.blood_group_type = '-ve';
        }
        localStorageService.set('Userdetails', response);



    });
    // }

    if ($scope.leave_balances == null) {
        UserDetails.get_leave_balance().then(function(response) {
            $scope.leave_balances = response;
            localStorageService.set('leaveBalance', response);
        })


    }


    // PendingLeaveRequest.get_pending_request(loginInfo, current_date).then(function(response) {
    //     $scope.PendingleaveRequest = response;
    //     localStorageService.set("pendingLeaveRequest", response)


    // });
    // RecentLeaveRecords.get_recent_records(loginInfo, current_date).then(function(response) {
    //     $scope.recentLeaveRecords = response;
    //     localStorageService.set("recentLeaveRecords", response);
    //     // console.log($scope.recentLeaveRecords);
    // });

    $scope.userInfo = {};
    if (localStorageService.get('EmployeeList') == null) {
        UserDetails.get_assistant_employees().then(function(response) {
            // console.log(response);


            if (response) {
                // console.log(response);
                localStorageService.set('EmployeeList', response);
                $scope.userInfo.hasEmployee = 'yes';

                /* removed as of now count */
                /*UserDetails.get_assistant_employee_leaveList().then(function (response) {
                 $scope.loginInfo.employeeLeaveList = response.length;
                 });

                 UserDetails.get_assistant_employee_gatePassList().then(function (response) {
                 $scope.loginInfo.employeeGatePassList = response.length;
                 });*/
                /* removed as of now */
            }
        })
    } else {
        $scope.userInfo.hasEmployee = 'yes';

    }

}]);


app.controller('RequestLeaveCtrl', ['$scope', 'RequestLeave', '$state', 'localStorageService', '$ionicPlatform', 'ionicDatePicker', 'ionicTimePicker', '$ionicPopup', '$ionicHistory', function($scope, RequestLeave, $state, localStorageService, $ionicPlatform, ionicDatePicker, ionicTimePicker, $ionicPopup, $ionicHistory) {

    $scope.startDate = {
        callback: function(val) { //Mandatory

            var start_date = convert(new Date(val));
            angular.element(document.querySelector('#leave_start_date')).val(start_date);
            $scope.endDate.from = start_date;

            $scope.formData.leave_start_date = angular.element(document.querySelector('#leave_start_date')).val();
            console.log($scope.formData);
            if ($("#leave_end_date").val()) {
                var from_date = new Date($("#leave_start_date").val());
                var to_date = new Date($("#leave_end_date").val());
                if (from_date > to_date) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: " Start Date can\'t be greater than end date"
                    });
                    return false;
                }

                $scope.formData.no_of_days = calculateNosofDays(to_date, from_date);
                angular.element(document.querySelector('#noOfDay')).val($scope.formData.no_of_days);

                // $('#noOfDay').val();
            }

            if ($("#endTime").val() && $("#startTime").val() && $("#leave_end_date").val()) {
                var diff = calculateDiffinTime12Hr($("#leave_start_date").val(), $("#leave_end_date").val(), $("#startTime").val(), $("#endTime").val());
                $('#min-time').val(diff + ' mins');
            }


        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
    };



    $scope.openDatePicker = function() {

        ionicDatePicker.openDatePicker($scope.startDate);

    };





    $scope.endDate = {
        callback: function(val) { //Mandatory
            var end_date = convert(new Date(val));
            angular.element(document.querySelector('#leave_end_date')).val(end_date);
            $scope.formData.leave_end_date = angular.element(document.querySelector('#leave_end_date')).val();
            if ($("#leave_start_date").val()) {
                var from_date = new Date($("#leave_start_date").val());
                var to_date = new Date($("#leave_end_date").val());
                var start = from_date.getTime();
                var end = to_date.getTime();
                if (start > end) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: " End Date can\'t be less than end Start Date"
                    });
                    return false;
                }
                $scope.formData.no_of_days = calculateNosofDays(to_date, from_date);
                angular.element(document.querySelector('#noOfDay')).val($scope.formData.no_of_days);
            }

            if ($("#endTime").val() && $("#startTime").val() && $("#leave_start_date").val()) {
                var diff = calculateDiffinTime12Hr($("#leave_start_date").val(), $("#leave_end_date").val(), $("#startTime").val(), $("#endTime").val());
                $('#min-time').val(diff + ' mins');
            }




        },
        inputDate: new Date(), //Optional
        from: new Date(), //disable all starting previous dates
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
    };

    $scope.openDatePicker2 = function() {

        ionicDatePicker.openDatePicker($scope.endDate);
    };

    var startTime = {
        callback: function(val) { //Mandatory
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {

                var t = (new Date).clearTime()
                    .addSeconds(val)
                    .toString('H:mm');

                var start_time = timeTo12HrFormat(t);
                angular.element(document.querySelector('#startTime')).val(start_time);
                if ($("#endTime").val() && $("#leave_start_date").val() && $("#leave_end_date").val()) {
                    var diff = calculateDiffinTime12Hr($("#leave_start_date").val(), $("#leave_end_date").val(), $("#startTime").val(), $("#endTime").val());
                    // var start = angular.element(document.querySelector('#startTime')).val().split(':');
                    // var end = angular.element(document.querySelector('#endTime')).val().split(':');
                    // var diff = calculateDiffinTime(end, start);
                    $('#min-time').val(diff + ' mins');


                }

            }
        }

    }
    $scope.openTimePicker = function() {
        ionicTimePicker.openTimePicker(startTime);

    }

    var endTime = {
        callback: function(val) { //Mandatory
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {
                var t = (new Date).clearTime()
                    .addSeconds(val)
                    .toString('H:mm');

                var end_time = timeTo12HrFormat(t);
                angular.element(document.querySelector('#endTime')).val(end_time);
                if ($("#startTime").val() && $("#leave_start_date").val() && $("#leave_end_date").val()) {
                    var diff = calculateDiffinTime12Hr($("#leave_start_date").val(), $("#leave_end_date").val(), $("#startTime").val(), $("#endTime").val());


                    // var start = angular.element(document.querySelector('#startTime')).val().split(':');
                    // var end = angular.element(document.querySelector('#endTime')).val().split(':');
                    // var diff = calculateDiffinTime(end, start);
                    $('#min-time').val(diff + ' mins');


                }


            }
        }

    }
    $scope.openTimePicker2 = function() {
        ionicTimePicker.openTimePicker(endTime);

    }

    //date converter function

    function convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }




    //on change function for employee list
    $scope.getValueEmployee = function(employee) {
        var emp_code = $scope.formData.EmployeeCode;
        var emp_name = $.grep($scope.employeeList, function(employee) {
            return employee.employee_code == emp_code;
        })[0].full_name;
        var leave_code = angular.element(document.querySelector('#leave_type')).val();
        // console.log('employye '+leave_code);

        if (emp_code != '' && leave_code != '' && emp_code != undefined && leave_code != undefined) {

            var leave_request_nos = localStorageService.get("RequestNo").Leave_Request_No;



            RequestLeave.get_on_hand_leave(loginInfo, leave_request_nos, leave_code, emp_code).then(function(response) {
                $scope.on_Hand_LeaveField = response;
                console.log(response);
                angular.element(document.querySelector('#on_hand_leave')).val(response);
                // $("#on_hand_leave").val(response);
            })
        }


    }

    $scope.getValueLeaveType = function() {
        var leave_code = angular.element(document.querySelector('#leave_type')).val();
        var emp_code = angular.element(document.querySelector('#employee_code')).val();
        if (emp_code != '' && leave_code != '' && emp_code != undefined && leave_code != undefined) {
            var leave_request_nos = localStorageService.get("RequestNo").Leave_Request_No;

            RequestLeave.get_on_hand_leave(loginInfo, leave_request_nos, leave_code, emp_code).then(function(response) {
                $scope.on_Hand_LeaveField = response;
                // console.log(response);
                angular.element(document.querySelector('#on_hand_leave')).val(response);
                // $("#on_hand_leave").val(response);
            })
        }

    }

    /* call api to get on hand leave based on employee code and leave code */
    $scope.getSubType = function() {
        var leave_request_nos = localStorageService.get("RequestNo").Leave_Request_No;
        var sub_leave_code = angular.element(document.querySelector('#sub_leave_type')).val();

        RequestLeave.get_leave_detail(loginInfo, leave_request_nos, sub_leave_code).then(function(response) {
            // console.log(response);
            var startdate = response.Leave_Start_Date.split("T")[0];
            var enddate = response.Leave_End_Date.split("T")[0];
            var starttime = response.Leave_Start_Time;
            var endtime = response.Leave_End_Time;
            angular.element(document.querySelector('#leave_start_date')).val(startdate);
            angular.element(document.querySelector('#leave_end_date')).val(enddate);
            angular.element(document.querySelector('#startTime')).val(starttime);
            angular.element(document.querySelector('#endTime')).val(endtime);





            var from_date = new Date(startdate);
            var to_date = new Date(enddate);
            $scope.formData.no_of_days = calculateNosofDays(to_date, from_date);
            // $scope.formData.no_of_days = response.no_of_daysField;
            var diff = calculateDiffinTime12Hr(startdate, enddate, starttime, endtime);

            // var start = response.leave_Start_TimeField.split("T")[1].split(':');
            // var end = response.leave_End_TimeField.split("T")[1].split(':');
            // var diff = calculateDiffinTime(end, start);
            angular.element(document.querySelector('#min-time')).val(diff + ' mins');

        })


    }





    var loginInfo = localStorageService.get('LoginInfo');
    RequestLeave.get_request_form(loginInfo).then(function(response) {

        localStorageService.set("requestData", response);
        $scope.Leave_Type_Code = response.leave_type_code;
        $scope.Leave_Type = response.leave_type;


        $scope.employeeList = localStorageService.get('EmployeeList');


    });



    RequestLeave.get_request_no(loginInfo).then(function(response) {
        $scope.formData = {};
        var loginInfo = localStorageService.get('LoginInfo');
        $scope.formData = response;

        // console.log($scope.formData);
        localStorageService.set("RequestNo", response);

        $scope.annual_leave_sub_type = [{ code: '3', name: 'Full Day Leave' }, { code: '1', name: 'First Half Leave' }, { code: '2', name: 'Second Half Leave' }]
            // console.log(response);


        $scope.LeaveRequestSubmit = function(isValid) {
            if (isValid) {

                if (parseFloat($("#on_hand_leave").val()) < parseFloat($scope.formData.no_of_days)) {
                    $ionicPopup.alert({
                        title: 'Info!',
                        template: 'You do not have enough leave on hand!'
                    })

                } else {
                    var strt_dte = angular.element(document.querySelector('#leave_start_date')).val();
                    var end_dte = angular.element(document.querySelector('#leave_end_date')).val();

                    if (strt_dte == '' || end_dte == '') {
                        $ionicPopup.alert({
                            title: 'Error!',
                            template: "Start Date and/or End Date can\'t be empty"
                        });
                        return false;


                    } else {
                        $scope.formData.access_token = loginInfo.access_token;
                        $scope.formData.Leave_Type = angular.element(document.querySelector('#leave_type')).val();
                        $scope.formData.Sub_Leave_Type = angular.element(document.querySelector('#sub_leave_type')).val();
                        $scope.formData.Start_Date = strt_dte;
                        $scope.formData.End_Date = end_dte;
                        $scope.formData.Start_Time = angular.element(document.querySelector('#startTime')).val();
                        $scope.formData.End_Time = angular.element(document.querySelector('#endTime')).val();





                        RequestLeave.post_leave($scope.formData).then(function(response) {



                            if (response == 1) {
                                $ionicPopup.alert({
                                    title: 'Request Sent!',
                                    template: 'Your request has been submitted for admin approval!'
                                }).then(function(res) {

                                    $state.go('app.leave-history');

                                });



                            } else {
                                $ionicPopup.alert({
                                    title: 'Error!',
                                    template: response.Message
                                }).then(function() {
                                    console.log('error');

                                });

                            }
                        });

                    }





                }


            } else {
                $scope.submitted = true;
            }

        }

    });

    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();


}]);

app.controller('PendingLeaveRequestCtrl', ['$scope', '$rootScope', '$stateParams', 'localStorageService', 'UserDetails', '$ionicPopup', '$state', '$route', '$ionicLoading', function($scope, $rootScope, $stateParams, localStorageService, UserDetails, $ionicPopup, $state, $route, $ionicLoading) {


    if (!$stateParams.emp_id) {
        //for ionic loader
        $scope.show = function() {
            $ionicLoading.show({
                template: '<p>Loading...</p><ion-spinner></ion-spinner>'
            });
        };

        $scope.hide = function() {
            $ionicLoading.hide();
        };

        $scope.numberOfItemsToDisplay = 10;
        $scope.pendingRequestlists = getData();
       

        function getData() {
            var a = [];
            $scope.show($ionicLoading);
            UserDetails.get_assistant_employee_leaveList().then(function(response) {

                angular.forEach(response, function(item) {
                    a.push(item);

                });


            }).finally(function($ionicLoading) {

                $scope.hide($ionicLoading);
            });

            return a;
        }


        $scope.loadMore = function() {

            if ($scope.pendingRequestlists.length > $scope.numberOfItemsToDisplay)
                $scope.numberOfItemsToDisplay += 10; // load 20 more items
        }

    }


    


    // $scope.numberOfItemsToDisplay = 10;
    // $scope.noMoreItemsAvailable = false;
    // $scope.pendingRequestlists = [];
    // $scope.loadMore = function() {
    //     UserDetails.get_assistant_employee_leaveList().then(function(response) {
    //         angular.forEach(response, function(item) {
    //             $scope.pendingRequestlists.push(item);

    //         });      });

    //     if($scope.pendingRequestlists.length > $scope.numberOfItemsToDisplay)
    //     {
    //          $scope.numberOfItemsToDisplay += 10; 
    //           $scope.noMoreItemsAvailable = true;


    //     }



    //     $scope.$broadcast('scroll.infiniteScrollComplete');
    // }

    /* detail of request-approval */
    if ($stateParams.emp_id != undefined) {
        UserDetails.get_pending_leaveRequest_byId($stateParams.emp_id).then(function(response) {

            $rootScope.leaveapprovalList = response;
        })
    }
    /* detail of request-approval */

    $scope.Approval = function(req_no, status, remarks) {

        if (remarks == undefined) {
            var remarks = "";
        }



        UserDetails.leaveRequestApprroveDisapprove(req_no, status, remarks).then(function(response) {

            if (response == true) {
                $ionicPopup.alert({
                        title: 'Success!',
                        template: 'Successfully ' + status + ' application'
                    })
                    .then(function(res) {

                        $state.go('app.pending-leave-request');


                    });
            } else {
                $ionicPopup.alert({
                    title: 'Error!',
                    template: response.Message
                });
            }
        });
    }





}]);





app.controller('leaveHistoryCtrl', ['$scope', 'localStorageService', 'LeaveHistory', '$ionicLoading', '$http', function($scope, localStorageService, LeaveHistory, $ionicLoading, $http) {
    var user_id = localStorageService.get('LoginInfo');
    $scope.leaveHistory = [];
    $scope.show = function() {
        $ionicLoading.show({
            template: '<p>Loading...</p><ion-spinner></ion-spinner>'
        });
    };

    $scope.hide = function() {
        $ionicLoading.hide();
    };

    $scope.show($ionicLoading);
    LeaveHistory.get_history(user_id).then(function(response) {
        angular.forEach(response, function(item) {
            $scope.leaveHistory.push(item);

        });
        localStorageService.set('leaveHistoryRecords', response);
    }).finally(function($ionicLoading) {

        $scope.hide($ionicLoading);
    });








    $scope.doRefresh = function() {
        localStorageService.remove('leaveHistoryRecords');
        $scope.leaveHistory=[];
         $scope.show($ionicLoading);
        $http({
                method: 'GET',
                headers: { Authorization: 'Bearer ' + user_id.access_token },
                url: getBaseUrl() + 'api/LeaveHistory',

            })
            .success(function(data) {
                angular.forEach(data, function(item) {

                    $scope.leaveHistory.push(item);

                });
                localStorageService.set('leaveHistoryRecords',data);
})
            .error(function(err) {
                alert(err);
            }).finally(function($ionicLoading) {
        $scope.hide($ionicLoading);

    });        
            console.log($scope.leaveHistory.length);
        $scope.$broadcast('scroll.refreshComplete');


    }







}]);
