'use strict'
var app = angular.module('starter');
app.controller('RequestOddTrainingCtrl', ['$scope', '$state', 'RequestOddTraining', 'NepaliEnglishDateConverter', 'localStorageService', 'ionicDatePicker', 'ionicTimePicker', '$ionicPopup', function($scope, $state, RequestOddTraining, NepaliEnglishDateConverter, localStorageService, ionicDatePicker, ionicTimePicker, $ionicPopup) {


    var date = new Date();
    var current_date = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
    /* convert eng to nep */
    $scope.converter = new NepaliEnglishDateConverter();

    var curEngDate = current_date.split('/');
    var year = curEngDate[2];
    var month = curEngDate[0];
    var day = curEngDate[1];


    var cur_nep_date = $scope.converter.eng_to_nep(
        Number(year),
        Number(month),
        Number(day)
    );
    $scope.cur_nep_date = cur_nep_date.year + '/' + cur_nep_date.month + '/' + cur_nep_date.date;
    $scope.entrydata = {};

    //ionic datepicker start
    $scope.OddStartDate = {
        inputDate: new Date(),
        from: new Date(),
        callback: function(val) {
            var start_date = convert(new Date(val));
            angular.element(document.querySelector('#StartDate')).val(start_date);
            $scope.entrydata.StartDate = angular.element(document.querySelector('#StartDate')).val();
            $scope.OddEndDate.from = start_date;
            if (angular.element(document.querySelector('#EndDate')).val()) {
                var frm_date = angular.element(document.querySelector('#StartDate')).val();
                var end_date = angular.element(document.querySelector('#EndDate')).val();
                // console.log(frm_date);
                // console.log(end_date);
                if (frm_date > end_date) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: " Start Date can\'t be greater than end date"
                    }).then(function() {
                        // angular.element(document.querySelector('#StartDate')).val('');

                    })
                }
            }

            if ($("#StartTime").val() && $("#EndTime").val() && $("#EndDate").val()) {
                var diff = calculateDiffinTime12Hr($("#StartDate").val(), $("#EndDate").val(), $("#StartTime").val(), $("#EndTime").val());
                $('#min-time').val(diff + ' mins');
            }

        }
    }
    $scope.openStartDatePicker = function() {

        ionicDatePicker.openDatePicker($scope.OddStartDate);

    }

    $scope.OddEndDate = {
        inputDate: new Date(),
        from: new Date(),
        callback: function(val) {
            var end_date = convert(new Date(val));
            angular.element(document.querySelector('#EndDate')).val(end_date);
            $scope.entrydata.EndDate = angular.element(document.querySelector('#EndDate')).val();
            if (angular.element(document.querySelector('#StartDate')).val()) {
                var frm_date = angular.element(document.querySelector('#StartDate')).val();
                var end_date = angular.element(document.querySelector('#EndDate')).val();
                // console.log(frm_date);
                // console.log(end_date);
                if (frm_date > end_date) {
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: " End Date can\'t be less than end Start Date"
                    }).then(function() {
                        // angular.element(document.querySelector('#EndDate')).val('');


                    })
                }
            }


            if ($("#StartTime").val() && $("#EndTime").val() && $("#StartDate").val()) {
                var diff = calculateDiffinTime12Hr($("#StartDate").val(), $("#EndDate").val(), $("#StartTime").val(), $("#EndTime").val());
                $('#min-time').val(diff + ' mins');
            }

        }
    }
    $scope.openEndDatePicker = function() {

        ionicDatePicker.openDatePicker($scope.OddEndDate);

    }

    function convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    //startTime
    var OddStartTime = {
        callback: function(val) { //Mandatory
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {

                var t = (new Date).clearTime()
                    .addSeconds(val)
                    .toString('H:mm');

                var start_time = timeTo12HrFormat(t); //convertine time to 12 hour format with AM/PM
                angular.element(document.querySelector('#StartTime')).val(start_time);
                $scope.entrydata.StartTime = angular.element(document.querySelector('#StartTime')).val();

                if ($("#EndTime").val() && $("#StartDate").val() && $("#EndDate").val()) {
                    var diff = calculateDiffinTime12Hr($("#StartDate").val(), $("#EndDate").val(), $("#StartTime").val(), $("#EndTime").val());
                    $('#min-time').val(diff + ' mins');
                }

            }
        }

    }
    $scope.openStartTimePicker = function() {
        ionicTimePicker.openTimePicker(OddStartTime);

    }

    //endTime
    var OddEndTime = {
        callback: function(val) { //Mandatory
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {
                var t = (new Date).clearTime()
                    .addSeconds(val)
                    .toString('H:mm');

                var end_time = timeTo12HrFormat(t); //convertine time to 12 hour format with AM/PM
                angular.element(document.querySelector('#EndTime')).val(end_time);
                $scope.entrydata.EndTime = angular.element(document.querySelector('#EndTime')).val();
                if ($("#StartTime").val() && $("#StartDate").val() && $("#EndDate").val()) {
                    var diff = calculateDiffinTime12Hr($("#StartDate").val(), $("#EndDate").val(), $("#StartTime").val(), $("#EndTime").val());
                    $('#min-time').val(diff + ' mins');

                }




            }
        }

    }
    $scope.openEndTimePicker = function() {
        ionicTimePicker.openTimePicker(OddEndTime);

    }
    $scope.employeeList = localStorageService.get('EmployeeList');
    // RequestOddTraining.get_request_form().then(function(response) {


    $scope.Types = [{ code: '1', name: 'ODD' }, { code: '2', name: 'Training' }];

    // });

    $scope.changeTypes = function() {

        var type_id = $scope.entrydata.Type;
        if (type_id != '') {
            RequestOddTraining.requestType(type_id).then(function(response) {
                $scope.entrydata.Entry_No = response.Entry_No;
                //$scope.entrydata.Employee_Code = response.employee_CodeField;
                $scope.entrydata.StartDate = response.StartDate.split('T')[0];
                $scope.entrydata.EndDate = response.EndDate.split('T')[0];
                $scope.entrydata.StartTime = response.StartTime;
                $scope.entrydata.EndTime = response.EndTime;
                $scope.entrydata.AdvanceAmount = response.AdvanceAmount;
                /* calculate the time required from start_time and end_time */
                var diff = calculateDiffinTime12Hr(response.StartDate.split('T')[0], response.EndDate.split('T')[0], response.StartTime, response.EndTime);

                $('#min-time').val(diff + ' mins');
                /* calculate the time required from start_time and end_time */
            })
        }
    }



    $scope.OddTrainigRequestSubmit = function(isValid) {
        if (isValid) {

            $scope.entrydata.Employee_Code = angular.element(document.querySelector('#employee_code')).val();
            RequestOddTraining.postOddTrainingData($scope.entrydata).then(function(response) {
                var newWindow = window.open();
                if (response == 1) {
                    $ionicPopup.alert({
                        title: 'Sent!',
                        template: "Your Request has been sent to the admin!"
                    }).then(function() {
                        $state.go('/pending-odd-training');
                    })


                } else {
                    $ionicPopup.alert({
                        title: 'Not Sent!',
                        template: response.Message
                    });

                }
            });

        } else {
            $scope.submitted = true;
        }
    }

}]);


app.controller('PendingOddTrainningCtrl', ['$scope', '$rootScope', '$stateParams', 'localStorageService', 'OddTrainingPending', '$state', '$ionicPopup', '$ionicLoading', '$ionicHistory', function($scope, $rootScope, $stateParams, localStorageService, OddTrainingPending, $state, $ionicPopup, $ionicLoading, $ionicHistory) {
    if (!$stateParams.emp_id) {
        //for ionic loader
        $scope.show = function() {
            $ionicLoading.show({
                template: '<p>Loading...</p><ion-spinner></ion-spinner>'
            });
        };

        $scope.hide = function() {
            $ionicLoading.hide();
        };

        $scope.numberOfItemsToDisplay = 10;
        $scope.pendingOddTrainingtlists = getData();

        function getData() {
            var a = [];
            $scope.show($ionicLoading);
            OddTrainingPending.get_PendingOddTrainingList().then(function(response) {

                angular.forEach(response, function(item) {
                    a.push(item);

                });
            }).finally(function($ionicLoading) {

                $scope.hide($ionicLoading);
            });

            return a;
        }

        // console.log($scope.pendingOddTrainingtlists);

        $scope.loadMore = function() {

            if ($scope.pendingOddTrainingtlists.length > $scope.numberOfItemsToDisplay)
                $scope.numberOfItemsToDisplay += 10; // load 20 more items
        }

    }


    /* detail of request-approval */
    if ($stateParams.emp_id != undefined) {
        OddTrainingPending.get_PendingOddTrainingbyId($stateParams.emp_id).then(function(response) {
            $scope.OddTrainingDetails = response;



            // $scope.MinTime = function(StartTime, End_time) {
            //     var start = StartTime.split(':');
            //     var end = End_time.split(':');
            //     start = parseInt(start[0] * 60) + parseInt(start[1]);
            //     end = parseInt(end[0] * 60) + parseInt(end[1]);
            //     return (end - start);
            // }
        });

    }


    $scope.Approval = function(req_no, status, remarks) {

        if (remarks == undefined) {
            var remarks = "";
        }

        OddTrainingPending.OddTrainingApprroveDisapprove(req_no, status, remarks).then(function(response) {

            if (response == true) {
                $ionicPopup.alert({
                        title: 'Success!',
                        template: 'Successfully ' + status + ' application'
                    })
                    .then(function(res) {
                        $ionicHistory.clearCache(); //pervious history clear garxa. need cache:false in StateProvider config

                        $state.go('app.pending-odd-trainning', $stateParams, { reload: true, inherit: false });
                        // $state.go('app.pending-odd-trainning');


                    });
            } else {
                $ionicPopup.alert({
                    title: 'Error!',
                    template: response.Message
                });
            }
        });
    }
}]);





app.controller('oddTrainningHistoryCtrl', ['$scope', 'localStorageService', 'OddTrainingHistory', '$ionicLoading','$http', function($scope, localStorageService, OddTrainingHistory, $ionicLoading,$http) {
    var employee_id = localStorageService.get('LoginInfo');

    $scope.OddTrainingHistorys = [];
    $scope.show = function() {
        $ionicLoading.show({
            template: '<p>Loading...</p><ion-spinner></ion-spinner>'
        });
    };

    $scope.hide = function() {
        $ionicLoading.hide();
    };
    $scope.show($ionicLoading);

    OddTrainingHistory.get_OddTraininghistory().then(function(response) {

        angular.forEach(response, function(item) {

            $scope.OddTrainingHistorys.push(item);
        });
        localStorageService.set('OddTrainingHistoryRecords', response);
    }).finally(function($ionicLoading) {

        $scope.hide($ionicLoading);
    });


    $scope.doRefresh = function() {
        localStorageService.remove('OddTrainingHistoryRecords');
        $scope.OddTrainingHistorys=[];
          $scope.show($ionicLoading);
        $http({
                method: 'GET',
                url: getBaseUrl() + 'api/ODDTrainingHistory',
                headers: { Authorization: 'Bearer ' + employee_id.access_token },
            })
            .success(function(data) {
                console.log(data);
                angular.forEach(data, function(item) {

                    $scope.OddTrainingHistorys.push(item);
                });
                   localStorageService.set('OddTrainingHistoryRecords',data);
            })
            .error(function(err) {
                deferred.resolve(err);
            }).finally(function($ionicLoading) {
        $scope.hide($ionicLoading);

    });          

        


        $scope.$broadcast('scroll.refreshComplete');


    };




}]);
