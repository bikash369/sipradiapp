angular.module('starter')
    .factory("RequestOddTraining", function($q, $http, localStorageService) {
        var credentails = localStorageService.get('LoginInfo');

        function get_request_form() {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: getBaseUrl() + 'api/GatePass',
                headers: { Authorization: 'Bearer ' + credentails.access_token }
            }).success(function mySucces(data) {
                deferred.resolve(data);
            }).error(function(err) {
                console.log(err);
            });
            return deferred.promise;
        }

        function postOddTrainingData(datas) {
           
            var deferred = $q.defer();
            var info = localStorageService.get('LoginInfo');
            $http({
                method: 'POST',
                url: getBaseUrl()+'api/ODDTraining/createODDTraining',
                headers:{ Authorization:'Bearer '+info.access_token},
                data: datas
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (err) {
                deferred.resolve(err);
            })
            return deferred.promise;

        }

        function requestType(request_type) {
            var deferred = $q.defer();

            $http({
                method: "GET",
                url: getBaseUrl() + 'api/ODDTraining?requestType=' + request_type,
                headers: { Authorization: 'Bearer ' + credentails.access_token }
            }).success(function mySucces(data) {
                deferred.resolve(data);
            }).error(function(err) {
                console.log(err);
            });
            return deferred.promise;
        }

        return {
            get_request_form: get_request_form,
            postOddTrainingData: postOddTrainingData,
            requestType: requestType
        }



    });

angular.module('starter')
    .factory("OddTrainingHistory", function($q, $http, localStorageService) {

        var credentails = localStorageService.get('LoginInfo');


        function get_OddTraininghistory() {
            var deferred = $q.defer();
            var OddTrainingHistory = localStorageService.get('OddTrainingHistoryRecords');
            if (OddTrainingHistory == null || OddTrainingHistory.length==0) {
                console.log("api call");
                $http({
                        method: 'GET',
                        url: getBaseUrl() + 'api/ODDTrainingHistory',
                        headers: { Authorization: 'Bearer ' + credentails.access_token },
                    })
                    .success(function(data) {
                        deferred.resolve(data);
                    })
                    .error(function(err) {
                        deferred.resolve(err);
                    });
            } else {
                console.log('no api');
                deferred.resolve(OddTrainingHistory);

            }

            return deferred.promise;
        }


        return {
            get_OddTraininghistory: get_OddTraininghistory
        }
    });

// angular.module('starter')
//     .factory("OddTrainingDetail", function($q, $http, localStorageService) {
//         var credentails = localStorageService.get('LoginInfo');

//         function get_OddTrainingDetail(entry_nos) {
//             var deferred = $q.defer();
//             $http({
//                     method: 'GET',
//                     url: getBaseUrl() + 'api/ODDTraining/ODDTrainingRequestByEntryNumber?entryNumber=' + entry_nos,
//                     headers: { Authorization: 'Bearer ' + credentails.access_token },
//                 })
//                 .success(function(data) {
//                     deferred.resolve(data);
//                 })
//                 .error(function(err) {
//                     deferred.resolve(err);
//                 })
//             return deferred.promise;
//         }

//         function get_EmployeeDetail(emp_code) {
//             var deferred = $q.defer();
//             $http({
//                     method: 'GET',
//                     url: getBaseUrl() + 'api/Employee/GetEmployeeDetail?employeeNumber=' + emp_code,
//                     headers: { Authorization: 'Bearer ' + credentails.access_token },
//                 })
//                 .success(function(data) {
//                     deferred.resolve(data);
//                 })
//                 .error(function(err) {
//                     deferred.resolve(err);
//                 })
//             return deferred.promise;
//         }

//         return {
//             get_OddTrainingDetail: get_OddTrainingDetail,
//             get_EmployeeDetail: get_EmployeeDetail
//         }
//     });

angular.module('starter')
    .factory("OddTrainingPending", function($q, $http, localStorageService) {
        var credentails = localStorageService.get('LoginInfo');

        function get_PendingOddTrainingList() {
            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: getBaseUrl() + '/api/PendingODDTrainingRequestOfEmployee',
                    headers: { Authorization: 'Bearer ' + credentails.access_token },
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function get_PendingOddTrainingbyId(entry_no) {
            var deferred = $q.defer();
            $http({
                    method: 'GET',
                    url: getBaseUrl() + '/api/PendingODDTrainingRequestOfEmployee/GetDetail?entryNumber=' + entry_no,
                    headers: { Authorization: 'Bearer ' + credentails.access_token },
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })
            return deferred.promise;
        }

        function OddTrainingApprroveDisapprove(Entry_No, status, remarks) {
            var deferred = $q.defer();
            if (status == 'Approve') {
                var url = getBaseUrl() + 'api/' + status + 'ODDTrainingRequest/' + status + '?entryNumber=' + Entry_No + '&remarks=' + remarks;
            } else {
                var url = getBaseUrl() + 'api/' + status + 'ODDTrainingRequest/' + status + '?entryNumber=' + Entry_No;
            }
            $http({
                    method: 'POST',
                    url: url,
                    headers: { Authorization: 'Bearer ' + credentails.access_token },
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(err) {
                    deferred.resolve(err);
                })

            return deferred.promise;


        }

        return {
            get_PendingOddTrainingList: get_PendingOddTrainingList,
            get_PendingOddTrainingbyId: get_PendingOddTrainingbyId,
            OddTrainingApprroveDisapprove: OddTrainingApprroveDisapprove
        }
    });
